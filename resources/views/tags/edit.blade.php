@extends('layouts.admin-panel.app')


@section('content')



    <div class="card">
        <div class="card-header m-0">
            Edit Tag
        </div>
        <div class="card-body">
            <form action="{{ route('tags.update', $tag->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control
                                                                    @error('name')is-invalid @enderror"
                        value="{{ old('name', $tag->name) }}" placeholder="Enter tag name">
                    @error('name')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-danger">Edit Tag</button>
            </form>
        </div>
    </div>
@endsection
