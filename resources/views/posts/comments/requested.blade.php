@extends('layouts.admin-panel.app')

@section('content')



    <div class="d-flex justify-content-end mb-3">
        <a href="" class="btn btn-outline-primary">Add comment</a>
    </div>
    <div class="card">
        <div class="card-header m-0">
            <h2>comments</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">User</th>
                        <th scope="col">Post</th>
                        <th scope="col">Body</th>
                        <th scope="col">Prior disapproval reason</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($comments as $comment)
                        <tr>
                            <td>{{ $comment->user->name }}</td>
                            <td> {{ $comment->post->title }} </td>
                            <td>{{ $comment->body }}</td>
                            <td></td>
                            <td>
                                @if ($comment->isApproved())
                                    <form action="{{ route('posts.disapprove-comment', $comment->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')

                                        <button type="submit" class="btn btn-sm btn-outline-danger">Disapprove</button>
                                    </form>

                                @else
                                    <form action="{{ route('posts.approve-comment', $comment->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')

                                        <button type="submit" class="btn btn-sm btn-outline-success">Approve</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="deletecomment" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        Are you sure you want to delete this comment?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delelte comment</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{ $comments->links('vendor.pagination.bootstrap-4') }}
@endsection

@section('page-level-scripts')
    <script>
        function displayModal(commentId) {
            var url = "/comments/trash/" + commentId;
            $("#deletecomment").attr('action', url);
        }
    </script>
@endsection
