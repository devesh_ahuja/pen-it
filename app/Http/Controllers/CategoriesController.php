<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {

        //The validation is done in CreateCategoryRequest

        Category::create([
            'name' => "$request->name"
        ]);
        session()->flash('success', 'Contact Created Successfully!'); //flash is a one-time request when the success key is accessed once, it gets deleted
        // Session::flash('success', 'Contact Created Successfully!'); Can do it this way also but this method is slower
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        //When we use Category $category it automatically binds the request id with the model mentioned in the request url like the request url in this case is categories/{category} so when the request comes in as categories/2 Laravel automatically binds the oject of category having id 2 here. This is known as request-model mapping

        $category->name = request()->name;
        // $category->update(['name' => request()->name,])
        $category->save();
        session()->flash('success', 'Category Updated Successfully');
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        if ($category->posts()->withTrashed()->count() > 0) {
            session()->flash('error', 'This category cannot be deleted!');
            return redirect(route('categories.index'));
        }

        $category->delete();

        session()->flash('error', 'Category deleted successfully!');

        return redirect(route('categories.index'));
    }
}
