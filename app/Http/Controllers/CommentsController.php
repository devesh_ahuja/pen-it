<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function commentRequest(Request $request, Post $post)
    {
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        auth()->user()->comments()->create([
            'body' => $request->body,
            'post_id' => $request->post_id

        ]);

        return redirect(route('blogs.show', $request->post_id));
    }

    public function storeReply(Request $request,  $post,  $comment)
    {
        Comment::create([
            'user_id' => auth()->user()->id,
            'post_id' => $post,
            'parent_id' => $comment,
            'body' => $request->body,
        ]);

        return redirect(route('blogs.show', $post));
    }



    public function requestedComments()
    {
        $comments = Comment::paginate(10);
        return view('posts.comments.requested', compact(['comments']));
    }

    public function approveComment(Comment $comment)
    {
        $comment->update(['approved_at' => now()]);
        session()->flash('success', 'Comment Approved Succesfully');
        return redirect(route('posts.comments-requests'));
    }
    public function disapproveComment(Comment $comment)
    {
        $comment->update(['approved_at' => NULL]);
        session()->flash('success', 'Comment Disapproved Succesfully');
        return redirect(route('posts.comments-requests'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
