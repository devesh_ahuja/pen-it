<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // php artisan migrate:fresh --seed command for migrating and seeding together
        $this->call(UserSeeder::class);
        $this->call(BlogSeeder::class);
    }
}
