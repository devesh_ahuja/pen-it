<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Devesh Ahuja',
            'email' => 'dahuja2514@gmail.com',
            'password' => Hash::make('devesh123'),
        ]);

        User::create([
            'name' => 'Ritika Ahuja',
            'email' => 'ritika2514@gmail.com',
            'password' => Hash::make('ritika123'),
        ]);
    }
}
