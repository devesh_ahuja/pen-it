<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;
use phpDocumentor\Reflection\Types\Resource_;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/blogs/{post}', [FrontendController::class, 'show'])->name('blogs.show');
Route::get('blogs/category/{category}', [FrontendController::class, 'category'])->name('blogs.category');
Route::get('blogs/tags/{tag}', [FrontendController::class, 'tag'])->name('blogs.tag');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';


//Application Routes

//Used raw url for categories.destroy


//Route Grouping
Route::middleware(['auth'])->group(function () {
    Route::resource('categories', CategoriesController::class);
    Route::resource('tags', TagsController::class);
    Route::post('posts/{post}/comments', [CommentsController::class, 'store'])->name('posts.create-comments');
    Route::post('posts/{post}/comments/{comment}/replies', [CommentsController::class, 'storeReply'])->name('posts.comments.replies');
    Route::get('posts/comments-requests', [CommentsController::class, 'requestedComments'])->name('posts.comments-requests');
    Route::put('posts/comments/{comment}/approve', [CommentsController::class, 'approveComment'])->name('posts.approve-comment');
    Route::put('posts/comments/{comment}/disapprove', [CommentsController::class, 'disapproveComment'])->name('posts.disapprove-comment');
    Route::delete('posts/trash/{post}', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('posts/trashed', [PostsController::class, 'trashed'])->name('posts.trashed');
    Route::put('posts/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');
    Route::get('posts/drafts', [PostsController::class, 'drafts'])->name('posts.drafts');
    Route::get('/posts/requests', [PostsController::class, 'requests'])->name('posts.approval-requests');
    Route::put('/posts/drafts/{post}/draft', [PostsController::class, 'draftPost'])->name('posts.draft-post');
    Route::put('posts/requests/{post}/approve', [PostsController::class, 'approveRequest'])->name('posts.approve-request');
    Route::put('posts/requests/{post}/disapprove', [PostsController::class, 'disapproveRequest'])->name('posts.disapprove-request');
    Route::put('posts/drafts/{post}/publish', [PostsController::class, 'publishDraft'])->name('posts.publish-draft');
    Route::resource('posts', PostsController::class);
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
    Route::put('/users/{user}/revoke-admin', [UsersController::class, 'revokeAdmin'])->name('users.revoke-admin');
});
